import React, { useState, Component } from 'react';
import { Image, StyleSheet, Button, TouchableOpacity, TouchableHighlight, Alert, ScrollView, } from 'react-native';
import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';
import appStyle from './appStyle'; //The universal style sheet, in this directory
import TabOneScreen from './TabOneScreen';
import themeChange from './appTheme' //For dark and reading mode
import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import Navigation from '../navigation';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { Overlay } from "react-native-elements";

export default function TabThreeScreen() {

  /**
   * Need this to navigate between screens:
   *    navigation.Navigate('ScreenName'), or
   *    navigation.goBack().
   * See BottomTabNavigator.tsx for bottom tab screen names. 
   */
  const navigation = useNavigation();

   /**
  * Account Information
  */
  const AccountInformation = () => {
    //Boolean for whether the AddItemOverlay is visible or not.
    const [accountInformationVisible, setAccountInformation] = useState(false);

    /**
     * Toggle RemoveItemOverlay
     */
    function toggleAccountInformation() {
      setAccountInformation(!accountInformationVisible);
    }

    /**
     * Overlay "html".
     */
    return (
      <Overlay
        overlayStyle={appStyle.buttonContainerOverlay}
        onBackdropPress={toggleAccountInformation}
        isVisible={accountInformationVisible}
      >
        <View>
          <Text style={{alignSelf:'center', fontSize: 20}}>Account Information</Text>
        <View>
          <Text style={{alignSelf:'center', fontSize: 20}}>Jeremy</Text>
        <View>
          <Text style={{alignSelf:'center', fontSize: 20}}>******</Text>
          </View>
        </View>
        </View>
      </Overlay>
    );
  };

    return (
      <View style = {appStyle.allScreen}>

        {/** DeleteList shown at the start */}
        <AccountInformation/> 

        <View style = {appStyle.headerBar}>
          <Ionicons size={50} style={{ marginBottom: -3 }} name='ios-arrow-dropleft-circle' color='white' onPress={() => navigation.goBack()}/>
          <Text style={appStyle.title}>Settings</Text>
          <Ionicons size={50} style={{ marginBottom: -3 }} name='ios-arrow-dropleft-circle' color='#70ad47'/>
        </View>

        <View style={styles.background}>
        <View style={styles.settingName}>
        <View style={styles.settingName}>
          <Text style={styles.settingNameText}>General</Text>
        </View>
          <TouchableHighlight
            underlayColor="#548235"
            style={styles.button}
            onPress={() => {}}
            >
            <Text style={styles.settingsButton}>Account Information</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.settingName}>
            <TouchableHighlight
            underlayColor="548235"
            style={styles.button}
            onPress={() => {}}
              >
            <Text style={styles.settingsButton}>Notifications</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.settingName}>
          <TouchableHighlight
            underlayColor="#548235"
            style={styles.button}
            onPress={() => {}}
            >
            <Text style={styles.settingsButton}>Privacy</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.settingName}>
          <TouchableHighlight
            underlayColor="#548235"
            style={styles.button}
            onPress={() => {}}
            >
            <Text style={styles.settingsButton}>Tutorial</Text>
            </TouchableHighlight>
          </View>
      <View style={{height: "100%", width: "100%"}}>
        <View style={styles.settingName}>
          <Text style={styles.settingNameText}>Font</Text>
        </View>
        <View style={styles.settingName}>
          <TouchableHighlight
            underlayColor="#548235"
            style={styles.button}
            onPress={() => {}}
            >
            <Text style={styles.settingsButton}>Comic Sans</Text>
            </TouchableHighlight>
          </View>
        <View style={styles.settingName}>
          <Text style={styles.settingNameText}>Language</Text>
        </View>
        <View style={styles.settingName}>
          <TouchableHighlight
            underlayColor="#548235"
            style={styles.button}
            onPress={() => {}}
            >
            <Text style={styles.settingsButton}>English (AUS)</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.settingName}>
          <TouchableHighlight
            underlayColor="#548235"
            style={styles.button}
            onPress={() => {}}
            >
            <Text style={styles.settingsButton}>English (UK)</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.settingName}>
          <TouchableHighlight
            underlayColor="#548235"
            style={styles.button}
            onPress={() => {}}
            >
            <Text style={styles.settingsButton}>English (US)</Text>
            </TouchableHighlight>
          </View>
        <View style={styles.settingName}>
          <Text style={styles.settingNameText}>Styles</Text>
        </View>
        <View style={styles.settingName}>
          <TouchableHighlight
            underlayColor="#548235"
            style={styles.button}
            onPress={() => {themeChange()}}
            >
            <Text style={styles.settingsButton}>Dark Mode</Text>
            </TouchableHighlight>
            </View>
          <View style={styles.settingName}>
          <TouchableHighlight
            underlayColor="#548235"
            style={styles.button}
            onPress={() => {}}
            >
            <Text style={styles.settingsButton}>Reading Mode</Text>
            </TouchableHighlight>
            </View>
          </View>
          </View>
      </View>
    );
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  settingName: {
    backgroundColor: "#a9d18e",
    width: "100%",
  },
  settingNameText: {
    color: "#548235",
    fontWeight: "bold",
    fontSize: 22,
    textAlign: "center",
    paddingVertical: 5,
  },

  button: {
    backgroundColor: "#548235",
    width: '80%',
    marginTop: 1,
    marginBottom: 2,
    borderRadius: 10,
    alignSelf: 'center',
    elevation: 5,
    paddingVertical: 5,
  },
  settingsButton: {
    fontSize: 18,
    textAlign: 'center',
    paddingVertical: 5,
  },
  background: {
    width: "100%",
    height: 1100,
    paddingTop: 1,
    backgroundColor: "#a9d18e",
  },
});
