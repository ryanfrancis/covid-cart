import * as React from 'react';
import { StyleSheet, Alert,  Dimensions, ScrollView, Image, Platform, Linking, Button, FlatList, TouchableOpacity, TouchableHighlight} from 'react-native';
import { colors, Overlay } from "react-native-elements";
import appStyle from './appStyle' //The universal style sheet, in this directory
import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { Text, View } from '../components/Themed';
import * as Location from 'expo-location';

//The geolib required for the implementation of static maps
const geolib = require('geolib');
//Temp
let once = true;

//The initial latitiude and longitude propsed
var intlat = -1.0;
var intlng = -1.0;

/**
   * Need this to navigate between screens:
   *    navigation.Navigate('ScreenName'), or
   *    navigation.goBack().
   * See BottomTabNavigator.tsx for bottom tab screen names. 
   */
export default function TabTwoScreen(this: any) {
  const [location, setLocation] = React.useState<Location.LocationObject | null>(null);
  const [errorMsg, setErrorMsg] = React.useState<String | null>(null);
  
  //Needed for navigiation
  const navigation = useNavigation();
  var GoogleStaticMap = require('react-native-google-static-map');
  var scrWidth = '' + Math.round(Dimensions.get('window').width);
  var scrHeight = '' + Math.round((Dimensions.get('window').height/2));
  //Flag for one time call to initialise lists.
  const [callOnceAtStart, disableStartCall] = React.useState(true);
  //store whose info is being displayed rn.
  const [currentStore, setCurrentStore] = React.useState(0);
  //Booleans sotring whether or not user is shopping at these types of stores
  const [atAldi, isAtAldi] = React.useState(false);
  const [atColes, isAtColes] = React.useState(false);
  const [atWoolworths, isAtWoolworths] = React.useState(false);
  //store info
  const [storeList, modifyStoreList] = React.useState(
    [
      {
        id: -1,
        name: "store1",
        address: "1 one st, Brisbane, QLD",
        latitude: -1.1,
        longitude: -1.1,
        phoneNumber: "0118 999 881 99 9119 7253",
        openingHous: "8am to 4pm",
        trafficLevel: "low foot traffic",
        wesbite: "https://doihaveinternet.com",
        distance: -1
      }
    ]
  );
    
  /**
   * Location of the user and permision requirements.
   */
  React.useEffect(() => {
    (async () => {
      let { status } = await Location.requestPermissionsAsync();
      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied');
      }

      let location = await Location.getCurrentPositionAsync({});
      setLocation(location);
    })();
  }, []);

  /**
   * Refresh all locations (both longitidue and latitide)
   */
  if (location && once) {
    once = false;
    intlat = location.coords.latitude;
    intlng = location.coords.longitude;
    refreshStores();
  }

  /**
   * Converts db format of stores to useable data structure.
   * @param stores - A list of store information in db format.
   */
  async function initialiseStores(stores: Array<Map<string, string>>) {
    //If no stores exist, do nothing.
    if (stores == undefined) {
      return;
    }
    
    var storeList : any = [];
    //Iterates through the each stores length to find the distance.
    for (var i = 0; i < stores.length; i++) {
      //Geolib is used to getDistance
      var distance = geolib.getDistance({
        latitude: intlat,
        longitude: intlng
      }, {
        latitude: parseFloat(JSON.parse(JSON.stringify(stores[i]))["latitude"]),
        longitude: parseFloat(JSON.parse(JSON.stringify(stores[i]))["longitude"])
      })
      var store = {
        id: -1,
        name: JSON.parse(JSON.stringify(stores[i]))["storeName"].toString(),
        address: JSON.parse(JSON.stringify(stores[i]))["address"].toString(),
        latitude: JSON.parse(JSON.stringify(stores[i]))["latitude"],
        longitude: JSON.parse(JSON.stringify(stores[i]))["longitude"],
        phoneNumber: JSON.parse(JSON.stringify(stores[i]))["phone"].toString(),
        openingHous: JSON.parse(JSON.stringify(stores[i]))["openingTimes"].toString(),
        trafficLevel: JSON.parse(JSON.stringify(stores[i]))["footTraffic"].toString(),
        wesbite: JSON.parse(JSON.stringify(stores[i]))["info"].toString(),
        distance: distance / 1000
      };
      storeList.push(store);
      // var storeTypes = ["coles", "woolworths", "aldi"];
      // for (var k = 0; k < 3; k++) {
      //   if (store.name == storeTypes[k]) {
      //     storeList.push(store);
      //   }
      // }
    }
    storeList.sort((a: any, b: any) => (a.distance > b.distance) ? 1 : -1)
    for (var i = 0; i < storeList.length; i++) {
      storeList[i].id = (i + 1).toString()
    }
    //storeList.sort(closerStore(store1, store2))
    //for (var i = 0; i < stores.length; i++) {storeList[i].id = (i + 1).toString()}
    modifyStoreList(storeList);
  }

  /**
   * Retrieves most recent store information and catches any unwanted errors
   * It also refreshes the list of stores and locations. 
   * Important! Also filters by searchBarContents. Leave that blank to retrieve all products.
   */
  async function refreshStores() {
    const db_getStores : any = await fetch(
      "https://deco3801-coding-at-the-disco.uqcloud.net/api/stores/get_stores.php"
    ).catch((error) => Alert.alert(error.toString()));
    const db_stores = await db_getStores.json();
    initialiseStores(db_stores["stores"]);
  }

  if (callOnceAtStart) {
    refreshStores();
    disableStartCall(false);
  }

  //Boolean for whether the StoreInfoOverlay is visible or not.
  const [storeInfoOverlayVisible, setStoreInfoOverlay] = React.useState(false);

  /**
   * Toggle StoreInfoOverlay
   */
  function toggleStoreInfoOverlay(id: number) {
    setStoreInfoOverlay(!storeInfoOverlayVisible);
    setCurrentStore(id);
  }

  /**
   * The overlay that contains all info for a given store, including: 
   *  - COVID saffety infromation
   *  - Foot Traffic
   *  - Open Times
   *  - View map
   */
  const StoreInfoOverlay = () => {
    return (
      <Overlay
        isVisible={storeInfoOverlayVisible}
        onBackdropPress={() => toggleStoreInfoOverlay(currentStore)}
        overlayStyle={styles.storeOverlay}
      >
        <View style={{backgroundColor: "transparent"}}>
          <Text style={styles.storeHeaderBanner}>{storeList[currentStore].name}</Text>
          <Text style={{color:'#8cbd6b', alignSelf:'center', marginTop: '3%', fontSize: 20}}>{storeList[currentStore].distance} km away</Text>
          <View style={styles.storeIconInfo}>
            <Ionicons
                size={50}
                style={styles.storeIconInfoIcon}
                name="md-pin"
              />
              <Text style={styles.storeIconInfoText}>{storeList[currentStore].address}</Text>
            </View>
          <View style={styles.storeIconInfo}>
            <Ionicons
                size={50}
                style={styles.storeIconInfoIcon}
                name="md-time"
              />
              <Text style={styles.storeIconInfoText}>Open {storeList[currentStore].openingHous}</Text>
          </View>
          <View style={styles.storeIconInfo}>
            <Ionicons
                size={50}
                style={styles.storeIconInfoIcon}
                name="md-call"
              />
              <Text style={styles.storeIconInfoText}>{storeList[currentStore].phoneNumber}</Text>
          </View>
          <View style={styles.storeBottomBanner}>
            <Text style={styles.storeBottomBannerText}>COVID safety information:</Text>
            <Text
              style={styles.healthText}
              onPress={() => Linking.openURL(storeList[currentStore].wesbite)}
            >
              {storeList[currentStore].wesbite}
            </Text>
            <Text style={styles.storeBottomBannerText}>Foot traffic: {storeList[currentStore].trafficLevel}</Text>
          </View>
          <View style={styles.closeOverlayButton}>
            <TouchableHighlight
                    onPress={() => {toggleStoreInfoOverlay(currentStore)}}
                  >
                    <Text style={styles.closeOverlayText}>View map</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Overlay>
    );
  }

  /**
   * The getDirections function uses an API fetch call which is checked vigourously for errors. 
   * 
   * @param long The longititude number of the directions.
   * @param lati The latititude number of the directions.
   * @param name Name of the intended dierection. 
   */
  function getDirections(long: number, lati: number, name: string) {
    var url = 'https://www.google.com/maps/dir/?api=1&dir_action=navigate&destination=' + name; // + lati + ',' + long;
    Linking.canOpenURL(url).then(supported => {
        if (!supported) {
            console.log('Can\'t handle url: ' + url);
        } else {
            return Linking.openURL(url);
        }
    }).catch(err => console.error('An error occurred', err)); 
  }

  /**
   * The openMap function which uses API fetch call commands to find the longititude, lagititde, and name of ht inteded direion. 
   * With which is long, lati that is checked vigourously for errors. 
   * 
   * @param long The longititude number of the directions.
   * @param lati The latititude number of the directions.
   * @param name Name of the intended dierection. 
   */
  function openMap(long: number, lati: number, name: string) {
    var murl = 'http://maps.google.com/maps?q=' + name; // + lati + ',' + long;
    Linking.canOpenURL(murl).then(supported => {
        if (!supported) {
            console.log('Can\'t handle url: ' + murl);
        } else {
            return Linking.openURL(murl);
        }
    }).catch(err => console.error('An error occurred', err));
  }

  //The return overlap which focuses on headers and acutaly design conecctionsd and ideals. 
  return (    
    <View style = {appStyle.allScreen}>
      <View style = {appStyle.headerBar}>
        <Ionicons size={50} style={{ marginBottom: -3 }} name='ios-arrow-dropleft-circle' color='white' onPress={() => navigation.goBack()}/>
        <Text style={appStyle.title}>Map</Text>
        <Ionicons size={50} style={{ marginBottom: -3 }} name='ios-arrow-dropleft-circle' color='#70ad47'/>
      </View>  

      <View style={styles.nearestStore}>
        <Text style={styles.nearestStoreText}>Nearest stores</Text>
      </View>
      <View style={styles.flatlist}>
      <FlatList
          data={storeList}
          keyExtractor={store => store.id.toString()}
          renderItem={({item}) => {
            return (
              <View style={styles.listContainer}>
                <View style={styles.listContainer}>
                  <Text onPress={() => toggleStoreInfoOverlay(item.id - 1)} style={styles.listText}>[&#8520;] {item.name}     </Text>
                </View>
                <View style={styles.listContainer}>
                  <TouchableHighlight
                    underlayColor="#548235"
                    style={styles.button}
                    onPress={() => {getDirections(item.longitude, item.latitude, item.name)}}
                  >
                    <Text style={styles.directionButton}>Get directions &#10162;</Text>
                  </TouchableHighlight>
                </View>
              </View>
            );
          }}
        />
      </View>

      {/*<ScrollView contentContainerStyle={{flexGrow: 1}}>
        Main section*/}
        
      {/*</ScrollView>*/}

      <StoreInfoOverlay/>
      <View style={styles.map}>  
        <TouchableOpacity activeOpacity = { 0.5 } onPress={() => openMap(storeList[currentStore].longitude, 
          storeList[currentStore].latitude, storeList[currentStore].name) }>
          <Image
            style={styles.map}
            source={{uri: 'https://maps.googleapis.com/maps/api/staticmap?center=' +
            storeList[currentStore].latitude + ',' + storeList[currentStore].longitude +
            '&zoom=14&scale=false&size=' +  
            scrWidth + 'x' +  scrHeight + 
            '&maptype=roadmap&key=AIzaSyBtLJz9iCKpPkkL8GxzthsWDH2l0kH_DaM&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xff0000%7Clabel:%7C' + 
            storeList[currentStore].latitude + ',' + storeList[currentStore].longitude}}
            //TODO: Could add custom store logo
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const locationProps = StyleSheet.create({
  
})

const styles = StyleSheet.create({
  //The style for container
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  //The style for title
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  }, 
  //The style for nearestStoreText
  nearestStoreText: {
    color: "white",
    fontWeight: "bold",
    fontSize: 22,
    textAlign: "center",
    paddingVertical: 5,
  },
  //The style for nearestStore
  nearestStore: {
    width: "100%",
    backgroundColor: "#548235",
    elevation: 6,
  },//The style for storeOverlay
  storeOverlay: {
    height: "72%",
    width: "90%",
    borderRadius: 20,
    elevation: 8,
  },
  //The style for storeheadbanner
  storeHeaderBanner: {
    marginTop:'5%',
    width: '106.2%',
    padding: '2%',
    backgroundColor: '#548235',
    color: 'white',
    alignSelf:'center',
    textAlign: 'center',
    fontSize: 21,
  },
  //The style for storeIconInfo:
  storeIconInfo: {
    flexDirection: "row",
    paddingHorizontal: "5%",
    marginVertical: "5%",
    width: "100%",
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
  },
  //The style for toreIconInfoText
  storeIconInfoText: {
    color: "black",
    width: "85%",
    fontSize: 18,
  },
  //The style for storeIconInfoIcon:
  storeIconInfoIcon: {
    marginRight: '6%',
    color: "#548235",
  },
  //The style for storeBottomBanner
  storeBottomBanner: {
    display: 'flex',
    width: '106.2%',
    padding: '2.5%',
    backgroundColor: '#a9d18e',
    alignItems: 'center', 
    alignSelf: "center",
  },
  //The style for storeBottomBanner
  storeBottomBannerText: {
    color: 'white',
    fontSize: 22,
    paddingVertical: 5,
  },
  //The style for healthText
  healthText: {
    color: 'blue', 
    textDecorationLine:'underline', 
    textAlign: "center", 
    fontSize: 16,
  },
  //The style for button
  button: {
    backgroundColor: "#548235",
    width: '80%',
    borderRadius: 10,
    alignSelf: 'center',
    elevation: 5,
  },
  //The style for directionButton
  directionButton: {
    fontSize: 18,
    textAlign: 'center',
    paddingVertical: 5,
  },
  //The style for closeOverlayButton
  closeOverlayButton: {
    marginTop: 15,
    backgroundColor: "#548235",
    width: '80%',
    borderRadius: 10,
    alignSelf: 'center',
    elevation: 5,
    color: '#548235',
    textAlign: 'center',
  },//The style for closeOverlayButton
  closeOverlayText: {
    fontSize: 18,
    textAlign: 'center',
    paddingVertical: 5,
  },
  ////The style for flatlist
  flatlist: {
    flexDirection: "row",
    height:'24%',
  },
  //The style for listContainer.
  listContainer: {
    backgroundColor: "#a9d18e",
    paddingBottom: 3,
  },
  //The style for listText
  listText: {
    color: "#548235",
    textAlign: "center",
    borderTopWidth: 3,
    borderTopColor: "#70ad47",
    fontSize: 20,
    paddingTop: 10,
    paddingBottom: 5,
    backgroundColor: "#a9d18e",
  },
  //The style for map.
  map: {
    paddingTop: 1,
    borderColor: "#70ad47",
    borderWidth: 1,
    backgroundColor: "#70ad47",
    width: '100%',
    height:'83%',
  },
});
