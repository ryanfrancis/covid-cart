import React from 'react';
import renderer from 'react-test-renderer';
import {render, fireEvent, waitFor, waitForElementToBeRemoved} from '@testing-library/react-native'
import TabTwoScreen from '../TabTwoScreen'

//npm run test --runInBand --detectOpenHandles

global.fetch = require('jest-fetch-mock');

/*
Need to Mock Navigation 'useNavigation' Hook as it fails test
if can't find a parent Navigation component.
*/
jest.mock('@react-navigation/native', () => {
    return {
        ...jest.requireActual('@react-navigation/native'),
        useNavigation: () => ({
            navigate: jest.fn(),
        }),
    };
});

describe(`Tab two testing`, () => {
    test(`Renders TabTwoScreen As Designed To`, () => {
        //Initialise screen
        const map_screen = renderer.create(<TabTwoScreen/>);
        /* 
        The screen should have rendered a map and shop list design/layout
        exactly like the saved snapshot.
        */
        expect(map_screen).toMatchSnapshot();
    });
});