import React from 'react';
import renderer from 'react-test-renderer';
import {render, fireEvent, waitFor, waitForElementToBeRemoved} from '@testing-library/react-native'
import TabThreeScreen from '../TabThreeScreen'

//npm run test --runInBand --detectOpenHandles

global.fetch = require('jest-fetch-mock');

/*
Need to Mock Navigation 'useNavigation' Hook as it fails test
if can't find a parent Navigation Component.
*/
jest.mock('@react-navigation/native', () => {
    return {
        ...jest.requireActual('@react-navigation/native'),
        useNavigation: () => ({
            navigate: jest.fn(),
        }),
    };
});

describe(`Tab three testing`, () => {
    test(`Renders TabThreeScreen As Designed To`, () => {
        //Initialise screen
        const settings_screen = renderer.create(<TabThreeScreen/>);
        /* 
        The screen should have rendered a settings page layout/design
        exactly like the saved snapshot.
        */
        expect(settings_screen).toMatchSnapshot();
    });
});