import React from 'react';
import renderer from 'react-test-renderer';
import {render, fireEvent, waitFor, waitForElementToBeRemoved, FireEventFunction} from '@testing-library/react-native'
import TabOneScreen from '../TabOneScreen'

//npm run test --runInBand --detectOpenHandles

global.fetch = require('jest-fetch-mock');

/**
 * Test suite of the major functions for the TabOneScreen (Home/Main page)
 */
describe(`Tab one testing`, () => {
    test(`Renders TabOneScreen As Designed To`, () => {
        //Initialise screen
        const login_screen = renderer.create(<TabOneScreen/>);
        /* 
        The screen should have rendered a login overlay exactly like the
        saved snapshot.
        */
        expect(login_screen).toMatchSnapshot();
    });

    test(`Fetches and Renders Items`, async () => {
        //Mock a list upon request
        fetch.mockResponseOnce(JSON.stringify({
            "lists":[{
                "listName":"tacos", "listID":"2"
            }]
        }));
        //Ignore Product's fetch request.
        fetch.mockResponseOnce();
        //Mock an item upon request.
        fetch.mockResponseOnce(JSON.stringify({
            "items":[{ 
                "productName": "spicy taco",
                "qty": "3",
                "ticked": "0",
                "productID": "1"
            }]
        }));
        //Test list fetch was indeed made.
        expect(global.fetch).toHaveBeenCalledWith("https://deco3801-coding-at-the-disco.uqcloud.net/api/lists/get_user_lists.php?username=Plo Koon");
        const {getAllByTestId} = render(<TabOneScreen/>);
        //Test there is now an item in the list
        await waitFor(()=>getAllByTestId("Item"));
    });

    test(`Remove Item`, async () => {
        //Mock a list upon request
        fetch.mockResponseOnce(JSON.stringify({
            "lists":[{
                "listName":"tacos", "listID":"2"
            }]
        }));
        //Ignore Product's fetch request.
        fetch.mockResponseOnce();
        //Mock an item upon request.
        fetch.mockResponseOnce(JSON.stringify({
            "items":[{ 
                "productName": "spicy taco",
                "qty": "3",
                "ticked": "1",
                "productID": "1"
            }]
        }));
        const {getAllByTestId, getByTestId, findByTestId, findAllByTestId} = render(<TabOneScreen/>);
        //Make sure there is now an item in the list
        const item = await findAllByTestId("Item");
        expect(item).toBeTruthy();
        //Press the remove item button 
        fireEvent.press(getByTestId("remove item button"));
        //Confirm want to remove item. 
        fireEvent.press(getByTestId("submit remove item button"));
        //App should have told db to remove item.
        expect(global.fetch).toHaveBeenNthCalledWith(9,
            "https://deco3801-coding-at-the-disco.uqcloud.net/api/items/remove_item_from_list.php",
            {"body":
                "{\"listID\":\"2\",\"productID\":\"1\"}",
                "headers": 
                    {"Content-Type": "application/json"},
                     "method": "POST"
            }
        );
        //Item should be gone from list.
        await (() => waitForElementToBeRemoved(item));
    });

    describe(`Product Screen Testing`, () => {
        //Test pressing the plus button did indeed switch screen.
        test(`Switches to product screen`, async () => {
            //Initialise screen
            const {getByTestId, findByText} = render(<TabOneScreen/>);
            //Press the add item button 
            fireEvent.press(getByTestId("add item button"));
            //Search for the page title, which should now be 'Search'.
            const newTitle = await findByText("Search");
            //Test that new header is indeed rendered.
            expect(newTitle).toBeTruthy();
        });
        //Simulate and test user adding an item
        test(`Add Item test`, async () => {
            //Mock a list upon request
            fetch.mockResponseOnce(JSON.stringify({
                "lists":[{
                    "listName":"tacos", "listID":"2"
                }]
            }));
            /*
            App will attempt to fetch products and refresh lists multiple more
            times, and seeing as how this test isn't interested in any other
            calls, the remaining fetch responses have all been mocked to
            receive products. Saves having to mock each individual call
            separately.
            */
            fetch.mockResponse(JSON.stringify({
                "products":[{ 
                    "productID": "2",
                    "productName": "spicyier taco",
                    "description": "the spiciest of tacos",
                    "atAldi": "1",
                    "atColes": "1",
                    "atWoolworths": "1",
                    "price": "1"
                }]
            }));
            //Initialise screen
            const {getByTestId, findByText, getByText, findByTestId} = render(<TabOneScreen/>);
            //Press the add item button
            fireEvent.press(getByTestId("add item button"));
            const newTitle = await findByText("Search");
            //Test that new header is indeed rendered.
            expect(newTitle).toBeTruthy();
            const taco = await findByTestId("select item");
            //Select the taco
            fireEvent.press(taco);
            //Submit taco
            fireEvent.press(getByTestId("submit item"));
            //App should have told db to add this item to user's current list
            expect(global.fetch).lastCalledWith(
                "https://deco3801-coding-at-the-disco.uqcloud.net/api/items/add_item_to_list.php",
                {"body":
                    "{\"listID\":\"2\",\"productID\":\"2\",\"qty\":\"1\",\"ticked\":\"0\"}",
                    "headers": 
                        {"Content-Type": "application/json"},
                         "method": "POST"
                }
            );
            //Should have switched back to view list screen.
            await (() => waitForElementToBeRemoved(newTitle));
        });
    });
});