import { StyleSheet, Dimensions } from "react-native";
import { withTheme } from "react-native-elements";

//Width of the current device's screen.
const width = Dimensions.get("window").width;

export default StyleSheet.create({
  allScreen: {
    backgroundColor: "#f2f2f2",
    alignItems: "center",
    flex: 1,
  },
  headerBar: {
    flex: 1,
    width: '100%',
    padding: '10%',
    paddingTop: '15%',
    alignItems: 'center',
    flexDirection: "row",
    justifyContent: 'space-between',
    backgroundColor: '#70ad47',
    elevation: 10,
    //Shadow effects work for IOS devices but not Android
    //shadowOffset:{  width: 10,  height: 10,  },
    //shadowColor: 'black',
    //shadowOpacity: 1.0,
    //shadowRadius: 1,
  },
  centralContainer: {
    justifyContent: 'flex-start',
    flexGrow: 1,
    width: width * 0.85,
    elevation: 5,
    backgroundColor: "#fffddd",
    marginBottom:'5%',
  },

  title: {
    fontSize: 30,
    color: "white",
  },

  /******************  Login Overlay styling *********************/

  loginOverlay: {
    display: "flex",
    height: "50%",
    width: "90%",
    borderRadius: 20,
    elevation: 8,
  },

  loginContainer: {
    display: "flex",
    width: "100%",
    flexDirection: "column",
    height: "100%",
    justifyContent: "space-around",
  },

  loginTitle: {
    fontSize: 80,
    fontWeight: "bold",
    width: "100%",
    textAlign: "center",
    color: "#70ad47",
  },

  loginInput: {
    alignSelf: "center",
    fontSize: 30,
    width: "90%",
    borderBottomWidth: 1,
    borderBottomColor: "#70ad47",
  },

  loginButtonsContainer: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
  },

  loginButton: {
    flexGrow: 1.5,
    backgroundColor: "#70ad47",
    borderWidth: 1,
    borderColor: "white",
    fontSize: 30,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    elevation: 5,
  },

  loginButtonText: {
    color: "white",
    fontSize: 30,
    textAlign: "center",
    paddingVertical: 5,
  },

  signUpButtonText: {
    color: "white",
    fontSize: 30,
    textAlign: "center",
    paddingVertical: 5,
  },

  signUpButton: {
    flexGrow: 1,
    backgroundColor: "#70ad47",
    borderWidth: 1,
    borderColor: "white",
    fontSize: 30,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    elevation: 5,
  },

  /******************  Login Overlay styling *********************/

  signUpOverlay: {
    display: "flex",
    height: "75%",
    width: "90%",
    borderRadius: 20,
    elevation: 8,
  },

  signUpContainer: {
    display: "flex",
    width: "100%",
    flexDirection: "column",
    height: "100%",
    justifyContent: "space-around",
  },

  signUpTitle: {
    fontSize: 80,
    fontWeight: "bold",
    width: "100%",
    textAlign: "center",
    color: "#70ad47",
  },

  signUpInput: {
    alignSelf: "center",
    fontSize: 30,
    width: "90%",
    borderBottomWidth: 1,
    borderBottomColor: "#70ad47",
  },

  signUpButtonsContainer: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
  },

  signUpButton2: {
    flexGrow: 1,
    backgroundColor: "#70ad47",
    borderWidth: 1,
    borderColor: "white",
    fontSize: 30,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    elevation: 5,
  },

  cancelSignUpButton: {
    flexGrow: 1.5,
    backgroundColor: "#70ad47",
    borderWidth: 1,
    borderColor: "white",
    fontSize: 30,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    elevation: 5,
  },

  cancelSignUpButtonText: {
    paddingVertical: 5,
    color: "white",
    fontSize: 30,
    textAlign: "center",
  },

  /******************  Welcome Mat Styling *********************/
  welcomeMatOverlay: {
    height: '90%',
    width: '90%',
    display: "flex",
    elevation: 8,
    borderRadius: 20,
  },

  welcomeMatContainer: {
    width: "100%",
    height: "100%",
    justifyContent: "space-between",
  },

  welcomeMatLogoContainer:{
    borderWidth: 1,
    borderColor: "black",
  },

  welcomeMatMainContentContainer:{
    flexGrow: 2,
    maxHeight: "70%",
    alignItems: "center"
  },

  welcomeMatMainImage:{
    height: "100%"
  },

  welcomeMatLogo:{
    width: '100%',
    height: "20%",  
  },

  welcomeMatButtonContainer: { 
    flexDirection: "row",
    justifyContent: "space-between",
    width: '100%',
    height: "10%"
  },

  welcomeMatButton: {
    color: "#70ad47"
  },

  /******************  List Dropdown Styling *********************/

  listDropDownOverlay: {
    alignSelf: "center",
    position: "absolute",
    top: 70,
    width: "80%",
    backgroundColor: "#70ad47",
    maxHeight: "100%",
  },

  penDropDownOverlay: {
    alignSelf: "center",
    position: "absolute",
    top: 70,
    width: "100%",
    backgroundColor: "#70ad47",
    maxHeight: "100%",
  },

  listDropDownContainer: {
    flex: 1,
    maxHeight: "100%",
  },

  listDropDownFlatList: {
    flexGrow: 1,
  },

  listDropDownItem: {
    height: 50,
  },

  listDropDownSeparator: {
    height: 1,
    width: "100%",
    backgroundColor: "white",
  },
  
  listDropDownText: {
    color: "white",
    fontSize: 25,
    textAlign: "center",
  },

  addListButton: {
    alignSelf: "center",
  },

  /****************** Create list styling *********************/

  createListOverlay: {
    width: "90%",
    position: "absolute",
    height: "40%",
    elevation: 5,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 0,
  },

  createListContainer: {
    display: "flex",
    width: "100%",
    flexDirection: "column",
    height: "100%",
    justifyContent: "space-around",
  },
  
  createListTitle: {
    fontSize: 30,
    textAlign: "center",
    width: "100%",
    backgroundColor: "#70ad47",
    color: "white",
  },

  createListInput: {
    alignSelf: "center",
    fontSize: 28,
    width: "90%",
    borderBottomWidth: 1,
    borderBottomColor: "#70ad47",
  },

  createListButtonContainer: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
  },

  createListSaveButton: {
    flexGrow: 1,
    backgroundColor: "#70ad47",
    borderWidth: 1,
    borderColor: "white",
    fontSize: 30,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    elevation: 5,
  },

  createListCancelButton: {
    flexGrow: 1,
    backgroundColor: "#70ad47",
    borderWidth: 1,
    borderColor: "white",
    fontSize: 25,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    elevation: 5,
  },

  createListSaveButtonText: {
    color: "white",
    fontSize: 30,
    textAlign: "center",
  },

  createListCancelButtonText: {
    color: "white",
    fontSize: 30,
    textAlign: "center",
  },

  /****************** Filter styling *********************/

  filterDropDownContainer: {
    alignSelf: "center",
    position: "absolute",
    width: "80%",
    backgroundColor: "white",
    borderRadius: 15,
    padding: "5%",
    flex: 1,
    elevation: 3
  },

  filterText: {
    fontWeight: "bold",
    padding: "5%",
    fontSize: 20,
    color: "#548235",
  },

  filterPriceRangeIcon: {
    alignSelf: "center",
    color: "#70ad47",
  },

  filterStoreContainer: {
    flex: 1,
    width: "100%",
    alignSelf: "center",
    justifyContent: "space-around",
    flexDirection: "row",
  },

  filterStoreSelected: {
    borderWidth: 0.5,
    borderColor: "#a9d18e",
    flexDirection: "row",
    justifyContent: "center",
    borderRadius: 5,
    backgroundColor: "#548235",
    padding: '3%', 
    elevation: 4,
  },

  filterStoreSelectedText: {
    textAlign: "center",
    fontSize: 15,
    color: "white",
  },

  filterStoreNotSelected: {
    borderWidth: 0.5,
    borderColor: "#548235",
    flexDirection: "row",
    justifyContent: "center",
    borderRadius: 5,
    backgroundColor: "white",
    padding: '3%',
    color: "white",
  },

  filterStoreNotSelectedText: {
    textAlign: "center",
    fontSize: 15,
    color: "#a9d18e",
  },

  filterPriceContainer: {
    flexDirection: "row",
    justifyContent: "center"
  },

  filterPriceInputContainer: {
    flexDirection: "row",
    minWidth: "20%",
    margin: 10,
  },

  filterPriceInput: {
    borderBottomWidth: 2,
    borderBottomColor: "#548235",
    borderStyle: "solid",
    flexDirection: "row",
    minWidth: "20%",
    fontSize: 30,
    color: "#70ad47",
    textAlign: "center",
  },

  filterPriceText: {
    color: "#70ad47",
    fontSize: 30,
  },

  filterApplyButton: {
    marginTop: 30,
    borderWidth: 0.5,
    borderColor: "#a9d18e",
    borderRadius: 20,
    elevation: 4,
    width: "100%",
    backgroundColor: "#548235",
    alignSelf: "center",
  },

  filterApplyButtonText: {
    textAlign: "center",
    color: "white",
    fontSize: 25,
    paddingVertical: "2%",
  },

  buttonContainerOverlay: {
    width: "75%",
    backgroundColor: "white",
    borderRadius: 10,
    padding: 15,
    elevation: 5,
  },

  buttonContainer: {
    marginTop: 8,
    
  },
  buttonContainerPen: {
    marginTop: 8,
    paddingVertical: 5,
  }
});
