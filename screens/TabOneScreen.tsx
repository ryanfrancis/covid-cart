import React, { useState, Component, useEffect, SetStateAction } from "react";
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Button,
  Alert,
  ScrollView,
  TextInput,
  FlatList,
  Dimensions,
  SafeAreaView,
  TouchableHighlight,
  ActivityIndicator,
} 
from "react-native";
import { Overlay } from "react-native-elements";
import appStyle from "./appStyle"; //The universal style sheet, in this directory.
import { Ionicons } from "@expo/vector-icons";
import { useNavigation, useNavigationState } from "@react-navigation/native";
//import {thing, a} from '../screens/SearchItemsScreen'
import SearchItemsScreen from "../screens/SearchItemsScreen";
import { SearchBar } from "react-native-elements";
import { Picker } from "react-native";
import { dismissAuthSession } from "expo-web-browser";
import CheckBox from '@react-native-community/checkbox';

export default function TabOneScreen() {
  /* 
  User's username, retrieved from login screen. 
  Initialised for dev purposes to Plo Koon till get login up and running.
  */
  const [username, setUsername] = useState("Plo Koon");

  //All a user's lists. (initialised w/ just a placeholder)
  const [allLists, overwriteMasterList] = useState(
    [
      [
        {
          name: "",
          quantity: -1,
          selected: false,
          creator: "",
          productID: -1
        }
      ]
    ]
  );

  /*
  Index of the current or 'active' list; the list that should be being
  displayed to user atm. Initialised to 0 till that API functionality is added.
  */
  const [currentList, switchList] = useState(0);

  //Flag for one time call to initialise lists.
  const [callOnceAtStart, disableStartCall] = useState(true);

  //Products to be displayed to user in 'search'
  const [productData, filterProductData] = useState(
    [
      {
        id: "1",
        name: "",
        description: "",
        atAldi: false,
        atColes: false,
        atWoolworths: false,
        price: -1, 
      }
    ]
  );

  //Width of the current device's screen.
  const width = Dimensions.get("window").width;

  /**
   * Converts db format products into usable data structure.
   * @param products
   */
  function initialiseProducts(products: Array<Map<string, string>>) {
    //Job done & dusted if no products in any stores anywhere.
    if (products == undefined) {
      return;
    }
    var productsList = [];
    for (var i = 0; i < products.length; i++) {
      productsList.push(
        {
          id: JSON.parse(JSON.stringify(products[i]))["productID"].toString(),
          name: JSON.parse(JSON.stringify(products[i]))["productName"].toString(),
          description: JSON.parse(JSON.stringify(products[i]))["description"].toString(),
          atAldi: JSON.parse(JSON.stringify(products[i]))["atAldi"] == 1,
          atColes: JSON.parse(JSON.stringify(products[i]))["atColes"] == 1,
          atWoolworths: JSON.parse(JSON.stringify(products[i]))["atWoolworths"] == 1,
          price: JSON.parse(JSON.stringify(products[i]))["price"]
        }
      );
    }
    //filter the data from the products list.
    filterProductData(productsList);
  }

  // Product Price Range  
  const [minPrice, setMinPrice] = useState(1);
  const [maxPrice, setMaxPrice] = useState(500);

  /**
   * Converts from db list format to app's format
   * @param lists - Lists for current user in format found in db.
   */
  async function initialiseLists(lists: Array<Map<string, string>>) {
    //Job done & dusted if no items in any of the user's lists
    if (lists == undefined) {
      return;
    }
    //initialise the format of the list
    var initialLists: SetStateAction<
      {
        name: string;
        quantity: number;
        selected: boolean;
        creator: string;
        productID: number;
      }[][]
    > = [];
    for (var i = 0; i < lists.length; i++) {
      var list = [];
      //First entry placeholde for list name.
      list.push({
        //I dont know why all this stuff needs to be attached to retrieve the info, but it does.
        name: JSON.parse(JSON.stringify(lists[i]))["listName"].toString(),
        quantity: -1,
        selected: false,
        creator: username,
        productID: JSON.parse(JSON.stringify(lists[i]))["listID"]
      });
      //Retrieve items for this list from db.
      const db_getItemsForList : any = await fetch(
        "https://deco3801-coding-at-the-disco.uqcloud.net/api/items/get_list_items.php?creator=" +
        username + 
        "&listID=" +
        JSON.parse(JSON.stringify(lists[i]))["listID"].toString()
      )
        .catch((error) => Alert.alert(error.toString()));
      const db_listItems = await db_getItemsForList.json();
      var items = db_listItems["items"];
      //If no items in this list start on next one.
      if (items == undefined) {
        initialLists.push(list);
        continue;
      }
      for (var k = 0; k < items.length; k++) {
        list.push({
          name: JSON.parse(JSON.stringify(items[k]))["productName"].toString(),
          quantity: parseInt(JSON.parse(JSON.stringify(items[k]))["qty"].toString()),
          selected: JSON.parse(JSON.stringify(items[k]))["ticked"] == 1,
          productID: JSON.parse(JSON.stringify(items[k]))["productID"],
          creator: username,
        });
      }
      initialLists.push(list);
    }
    overwriteMasterList(initialLists);
  }

  /**
   * Retrieves latest list info from db and updates app with it.
   */
  async function refreshLists() {
    const db_getLists : any = await fetch(
      "https://deco3801-coding-at-the-disco.uqcloud.net/api/lists/get_user_lists.php?username=" +
        username
    ).catch((error) => Alert.alert(error.toString()));
    const db_lists = await db_getLists.json();
    initialiseLists(db_lists["lists"]);
  }

  /**
   * Retrieves latest list info from db and updates app with it.
   * Important! Also filters by searchBarContents. Leave that blank to retrieve all products.
   * @param atAldi - true if want to search this store, false otherwise
   * @param atColes - true if want to search this store, false otherwise
   * @param atWoolworths - true if want to search this store, false otherwise
   */
  async function refreshProductsWithStores(atAldi: boolean, atColes: boolean, atWoolworths: boolean, minPrice: number, maxPrice: number) {
    const db_getProducts : any = await fetch(
      "https://deco3801-coding-at-the-disco.uqcloud.net/api/products/search_products.php?search=" + searchBarContents +
      "&atAldi=" + (atAldi == true ? "1" : "0") +
      "&atColes=" + (atColes == true ? "1" : "0") +
      "&atWoolworths=" + (atWoolworths == true ? "1" : "0") +
      "&maxPrice=" + (maxPrice) + 
      "&minPrice=" + (minPrice)
    ).catch((error) => Alert.alert(error.toString()));
    const db_products = await db_getProducts.json();
    if (db_products["message"] == "No matching items found." && productData.length > 0 && productData[0].price != -1) {
      filterProductData([]);
    } else {
      initialiseProducts(db_products["products"]);
    }
  }

  //Once of initial retrieval and setup of the list and product data from database
  if (callOnceAtStart) {
    refreshLists();
    refreshProductsWithStores(true, true, true, minPrice, maxPrice);
    disableStartCall(false);
  }

  /**
   * Changes the active list being displayed to specified/requested list.
   * @param name - name of list to change to.
   */
  function switchToList(name: string) {
    for (var i = 0; i < allLists.length; i++) {
      if (allLists[i][0].name == name) {
        switchList(i);
        return;
      }
    }
  }

  /**
   * Creates a blank new list with given name, then display that list.
   * Refreshes list info on page.
   * Important! Doesn't validate name. Must be done before calling.
   * @param name - Must not be name of a pre-existing list, otherwise won't switch to that list.
   */
  async function createBlankListWithName(name: string) {
    const db_addList : any = await fetch('https://deco3801-coding-at-the-disco.uqcloud.net/api/lists/add_list.php', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        },
      body: JSON.stringify({
        creator: username,
        listName: name
      })
    })
    //Catch and alert the user if the add list API fetch call failed.
    .catch((error) => Alert.alert(error.toString()));
    const db_response = await db_addList;
    if (db_response.status === 400) {
      Alert.alert("Add List fail");
    }
    //Refresh page to show updated list.
    refreshLists();
    switchToList(name);
  }

  /**
   * Checks to see if the current list has any items inside, if all clear then duplicate the current list.
   * Refreshes list info on page.
   * Important! Doesn't validate name. Must be done before calling.
   * @param name - Must not be name of a pre-existing list, otherwise won't switch to that list.
   */
  async function duplicateFilledList(name: string) {
    if (name == "") {
      Alert.alert("Can't have a blank list name!");
      return;
    }
    const db_duplicateList : any = await fetch('https://deco3801-coding-at-the-disco.uqcloud.net/api/lists/duplicate_list.php', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        listID: allLists[currentList][0].productID,
        newName: name
      })
    })
    //Catch and alert the user if the duplicate list API fetch call failed.
    .catch((error) => Alert.alert(error.toString()));
    const db_response = await db_duplicateList;
    if (db_response.status === 400) {
      Alert.alert("Duplicate List fail");
    }
    //Refresh page to show updated list.
    refreshLists();
    switchToList(name);
  }

  /**
   * Deletes the current list after checking if the user is sure. 
   * Refreshes list info on page.
   */
  async function deleteList() {
    if (allLists.length == 1) {
      Alert.alert("Must always have at least one list! Please create another list before deleteing this one.");
      return;
    }
    const db_deleteList : any = await fetch('https://deco3801-coding-at-the-disco.uqcloud.net/api/lists/delete_list.php ', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        },
      body: JSON.stringify({
        listID: allLists[currentList][0].productID
      })
    })
    //Catch and alert the user if the delete list API fetch call failed.
    .catch((error) => Alert.alert(error.toString()));
    const db_response = await db_deleteList;
    if (db_response.status === 400) {
      Alert.alert("Delete List fail");
    }
    //Switch to second last list.
    switchToList(allLists[allLists.length - 2][0].name);
    //Refresh page to show updated list.
    refreshLists();
  }

  /**
   * Add item to current list. Also Refreshes page afterwards.
   * @param productID - The ID of the product.
   * @param quantity - Quantity of the item. Must be a whole number > 0.
   */
  async function addItem(productID: string, quantity: number) {
    var checked = "0";
    const db_addItem : any = await fetch('https://deco3801-coding-at-the-disco.uqcloud.net/api/items/add_item_to_list.php', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        },
      body: JSON.stringify({
        listID: allLists[currentList][0].productID.toString(),
        productID: productID,
        qty: quantity.toString(),
        ticked: "0"
      })
    })
    //Catch and alert the user if the add or change item quantity API fetch call failed.
    .catch((error) => Alert.alert(error.toString()));
    const db_response = await db_addItem;
    if (db_response.status === 400) {
      Alert.alert("Add Item/Change Item Quantity fail");
    }
    //Refresh page to show updated list.
    refreshLists();
  }

  /**
   * Removed specified item from this list.
   * Important! Does not refresh list afterwards.
   * @param id - ID of the item to remove.
   */
  async function removeItem(id: string) {
    const db_removeItem : any = await fetch(
      "https://deco3801-coding-at-the-disco.uqcloud.net/api/items/remove_item_from_list.php",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
      body: JSON.stringify({
        listID: allLists[currentList][0].productID.toString(),
        productID: id,
      })
    })
    //Catch and alert the user if the remove specific item API fetch call failed.
    .catch((error) => Alert.alert(error.toString()));
    const db_response = await db_removeItem;
    if (db_response.status === 400) {
      Alert.alert("Remove Item fail");
    }
  }

  /*
   * Component that generates proper "html"-like code for each item,
   * its quantity and its selected status. Contains functions for modifying
   * those.
   */
  const listItems = Object.entries(allLists[currentList]).map(
    ([uselessThingy, item]) => {
      //Skip dummy first element in list
      if (item.quantity == -1) {
        return;
      }

    /**
     * Change the quantity of this item to this number.
     * Called when users use the picker.
     * @param newQuantity - The new value to change the quantity of the item.
     */
    function changeQuantity(newQuantity: number) {
      addItem(item.productID.toString(), newQuantity);
    }

      /**
       * Toggles the 'checked' state of an item, does this by rewriting the 'selected' boolean the item possesses.
       * Called when users tap the checkbox net to this item.
       */
      function toggleCheckbox() {
        //For each item in the active list:
        const newList = allLists[currentList].map((eachItem) => {
          //If it's the item we want to change:
          if (eachItem.name == item.name) {
            //create an otherwise identical item, but with updated 'select' status
            const updatedItem = {
              ...eachItem,
              selected: !item.selected,
            };
            //Replace old item in list with that
            return updatedItem;
          }
          //else don't modify item
          return eachItem;
        });
        //For each item in all lists:
        const newNewList = allLists.map((eachList) => {
          //If 'active' list:
          if (allLists.indexOf(eachList) == currentList) {
            //Replace old list with new modified one.
            return newList;
          }
          //else don't modify that list
          return eachList;
        });
        //change list
        overwriteMasterList(newNewList);
      }

      var myloop = [];
      for (let i = 1; i <= 10; i++) {
        myloop.push(<Picker.Item key={i} label={String(i)} value={i} />);
      }

      /**
       * The "html" of a given list item.
       *    View == div,
       *    Text == p,
       *    {} == access non-"html" stuff, so:
       *      style={access something from a stylesheet}, 'styles' is a stylesheet defined at the bottom of this doc if u want, or
       *      style={{direct css code here}}, or
       *      In any other case (that i know of rn) {} accesses this doc's normal code
       */
      return (
        <View testID={"Item"}
          style={item.selected ? styles.listActive : styles.list}
          key={item.name}
        >
          <CheckBox
            testID={"select item for removal"}
            value={item.selected}
            onValueChange={toggleCheckbox}
            style={styles.checkbox}
          />
          <View style={styles.item}>
            <Text style={styles.item}>{item.name}</Text>
          </View>
          <View style={styles.itemQuantity}>
              <Picker 
                selectedValue={item.quantity.valueOf()}
                style={styles.pickerStyle}
                onValueChange={(itemValue) => changeQuantity(itemValue)}
                mode={"dialog"}
              >
                {myloop}
              </Picker>
            </View>
        </View>
      );
    }
  );

  //Boolean for whether the AddItemOverlay is visible or not.
  const [addItemOverlayVisible, setAddItemOverlay] = useState(false);

  //Boolean for whether the RemoveItemOverlay is visible or not.
  const [removeItemOverlayVisible, setRemoveItemOverlay] = useState(false);

  //Var to store name of new item to be entered by user
  const [name, setName] = useState("");

  //Var to store id of new item to be entered by user
  const [id, setID] = useState("");

  /**
   * Toggle AddItemOverlay in/visibility or in/active status.
   *
   * Because this toggling happens through an onPress() call,
   * it needs a wrapper function to work and this func literally does nothing
   * else but that.
   */
  function toggleAddItemOverlay() {
    setAddItemOverlay(!addItemOverlayVisible);
  }

  /**
   * Toggle RemoveItemOverlay
   */
  function toggleRemoveItemOverlay() {
    setRemoveItemOverlay(!removeItemOverlayVisible);
  }

  /**
   * Toggle the add item overlay with preset name.
   * @param name - Name of the potential item to be added
   */
  function toggleAddItemOverlayWithNameAndID(name: string, ID: string) {
    setName(name);
    setID(ID);
    toggleAddItemOverlay();
  }

  //Var for contents of search bar
  const [searchBarContents, updateSearchContents] = useState("");

  /**
   * The overlay for adding items.
   */
  const AddItemOverlay = () => {
    //var to store quantity of new item to be entered by user. If user doesn't specify it's automatically set to one.
    const [quantity, setQuantity] = useState(1);

    /**
     * Adds inserted item to list.
     * Validates it first, then adds it to list, then dismisses overlay.
     */
    function validateThenAddItem() {
      //validate
      if (
        quantity == undefined ||
        quantity == NaN ||
        quantity < 1 ||
        name == ""
      ) {
        Alert.alert("Values invalid, please enter valid values");
        return;
      }
      for (var i = 0; i < allLists[currentList].length; i++) {
        if (allLists[currentList][i].name == name) {
          Alert.alert("Item already in List!");
          return;
        }
      }
      addItem(id, quantity);
      toggleAddItemOverlay();
      toggleeryoo();
    }

    /**
     * Overlay "html".
     */
    return (
      <Overlay
        overlayStyle={appStyle.buttonContainerOverlay}
        onBackdropPress={toggleAddItemOverlay}
        isVisible={addItemOverlayVisible}
      >
        <View>
          <Text style={styles.addItemText}>Add Item:</Text>
          <Text style={{color: "#548235", fontWeight:'bold', paddingBottom: 10}}>{name}</Text>
          <TextInput
            style={{ height: 40, borderWidth: 0.5, borderColor: "#548235", borderRadius: 15, padding: 5}}
            placeholder="Enter an item quantity"
            keyboardType='number-pad'
            onChangeText={(quantity) =>
              parseInt(quantity) == NaN ? 1 : setQuantity(parseInt(quantity))
            }
            value={undefined}
          />
          <View style={appStyle.buttonContainer}>
            <Button
              testID={"submit item"}
              title="Add" 
              onPress={validateThenAddItem}
              color="#70ad47"
            />
          <View style={appStyle.buttonContainer}></View>
            <Button
              title="Cancel"
              onPress={toggleAddItemOverlay}
              color="#548235"
            />
          </View>
        </View>
      </Overlay>
    );
  };

  /********************** LOGIN OVERLAY  *******************************/
  //Boolean for whether the login overlay is visible or not
  const [loginOverlayVisible, setLoginOverlay] = useState(true);

  /**
   * Wrapper function for above
   */
  function toggleLoginOverlay() {
    setLoginOverlay(!loginOverlayVisible);
  }

  /**
   * Store the toggleLoginOverylay function within the login function. 
   */
  function login() {
    toggleLoginOverlay();
  }

  /**
   * The Login overlay which pops up when loginOverlayVisible is true. 
   */
  const LoginOverlay = () => {
    return (
      <Overlay
        animationType="slide"
        transparent={true}
        isVisible={loginOverlayVisible}
        overlayStyle={appStyle.loginOverlay}
      >
        <SafeAreaView style={appStyle.loginContainer}>
          <Text style={appStyle.loginTitle}>Listable</Text>
          <TextInput
            // Username input
            style={appStyle.loginInput}
            placeholder="Username"
            placeholderTextColor="darkgray"
            onChangeText={(input) => storeNewListInput(input)}
          />
          <TextInput
            // password input
            style={appStyle.loginInput}
            placeholder="Password"
            placeholderTextColor="darkgray"
            secureTextEntry
            onChangeText={(input) => storeNewListInput(input)}
          />
          <View style={appStyle.loginButtonsContainer}>
            <TouchableHighlight
              // Login button
              underlayColor="#a9d18e"
              style={appStyle.loginButton}
              onPress={() => {
                toggleLoginOverlay();
                toggleWelcomeMat();
              }}
            >
              <Text style={appStyle.loginButtonText}>Login</Text>
            </TouchableHighlight>
            <TouchableHighlight
              // signup button
              underlayColor="#a9d18e"
              style={appStyle.signUpButton}
              onPress={() => {
                toggleLoginOverlay();
                toggleSignUpOverlay();
              }}
            >
              <Text style={appStyle.signUpButtonText}>Sign-up</Text>
            </TouchableHighlight>
          </View>
        </SafeAreaView>
      </Overlay>
    );
  };

  /********************** SIGN UP OVERLAY  *******************************/
  //Boolean for whether the sign up overlay is visible or not
  const [signUpOverlayVisible, setSignUpOverlay] = useState(false);

  /**
   * Wrapper function for above
   */
  function toggleSignUpOverlay() {
    setSignUpOverlay(!signUpOverlayVisible);
  }

  /** Variable to store the name of the account */
  var accountUsername = "";

  /** Function to store input from textInput view into variable above */
  function accountUsernameStore(input: string) {
    accountUsername = input;
  }

  /**
   * The SignUp overlay which pops up when signUpOverlayVisible is true. 
   */
  const SignUpOverlay = () => {
    return (
      <Overlay
        animationType="slide"
        transparent={true}
        isVisible={signUpOverlayVisible}
        overlayStyle={appStyle.signUpOverlay}
      >
        <SafeAreaView style={appStyle.signUpContainer}>
          <Text style={appStyle.signUpTitle}>Listable</Text>
          <TextInput
            // Name input
            style={appStyle.loginInput}
            placeholder="Name"
            placeholderTextColor="darkgray"
          />
          <TextInput
            // Email input
            style={appStyle.loginInput}
            placeholder="Email"
            placeholderTextColor="darkgray"
          />
          <TextInput
            // Username input
            style={appStyle.loginInput}
            placeholder="Username"
            placeholderTextColor="darkgray"
          />
          <TextInput
            // password input
            style={appStyle.loginInput}
            placeholder="Password"
            placeholderTextColor="darkgray"
            secureTextEntry
          />
          <View style={appStyle.signUpButtonsContainer}>
            <TouchableHighlight
              // Sign Up button
              underlayColor="#a9d18e"
              style={appStyle.signUpButton2}
              //underlayColor="#acc49b"
              onPress={() => toggleSignUpOverlay()}
            >
              <Text style={appStyle.signUpButtonText}>Sign-up</Text>
            </TouchableHighlight>

            <TouchableHighlight
              // Cancel button
              underlayColor="#a9d18e"
              style={appStyle.cancelSignUpButton}
              onPress={() => {
                toggleSignUpOverlay();
                toggleLoginOverlay();
              }}
              //underlayColor="#acc49b"
            >
              <Text style={appStyle.cancelSignUpButtonText}>Cancel</Text>
            </TouchableHighlight>
          </View>
        </SafeAreaView>
      </Overlay>
    );
  };

    /********************** WELCOME MAT  *******************************/
  //Boolean for whether the welcome mat is visible or not
  const [welcomeMatVisible, setWelcomeMat] = useState(false);

  const welcomeMatImages = [
    require('../assets/images/welcomeMatImages/1.png'),
    require('../assets/images/welcomeMatImages/2.png'),
    require('../assets/images/welcomeMatImages/3.png'),
    require('../assets/images/welcomeMatImages/4.png'),
  ];

  const [nextButton, setNextButton] = useState("md-arrow-forward");
  const [prevButtonColour, setPrevButtonColour] = useState("gray");

  const [imageCounter, setImageCounter] = useState(0); 

  /**
   * Wrapper function for above
   */
  function toggleWelcomeMat() {
    setPrevButtonColour("gray");
    setNextButton("md-arrow-forward");
    setImageCounter(0);
    setWelcomeMat(!welcomeMatVisible);
  }

  // Functions to iterate over tutorial images
  function nextImage() {
    setPrevButtonColour("#70ad47");
    if (imageCounter + 1 == welcomeMatImages.length) {
      toggleWelcomeMat();
    } else if ((imageCounter + 1) == (welcomeMatImages.length - 1)) {
      setNextButton("md-checkmark");
      setImageCounter(imageCounter + 1);
    } else {
      setImageCounter(imageCounter + 1);
    }
  };

  function prevImage() {
    setNextButton("md-arrow-forward");
    if (imageCounter > 0) {
      setImageCounter(imageCounter - 1);
    } 

    if (imageCounter == 0) {
      setPrevButtonColour("white");
    }
  };

  const WelcomeMat = () => {
    return (
      <Overlay
        animationType="slide"
        transparent={true}
        isVisible={welcomeMatVisible}
        overlayStyle={appStyle.welcomeMatOverlay}
      >
        <SafeAreaView style={appStyle.welcomeMatContainer}>
          {/*Logo Header*/}
            <Image source={require('../assets/images/logoStaple.png')}
              style={appStyle.welcomeMatLogo}
              resizeMode={"contain"}/> 


          {/*Main image section*/}

          <View style={appStyle.welcomeMatMainContentContainer}>
            <Image
            source={welcomeMatImages[imageCounter]}
            resizeMode={"contain"}
            style={appStyle.welcomeMatMainImage}
            />
          </View>
          
          {/*Bottom button footer*/}

          <View style={appStyle.welcomeMatButtonContainer}>

            <TouchableHighlight
                // Back button
                onPress={prevImage}
              >
                <Ionicons
                size={50}
                name="md-arrow-back"
                color={prevButtonColour}
              />
              </TouchableHighlight>

              <TouchableHighlight
                // Next button
                onPress={nextImage}
              >
                <Ionicons
                size={50}
                name={nextButton}
                color="#70ad47"
              />
              </TouchableHighlight>
          </View>
        </SafeAreaView>
      </Overlay>
    );
  };

  /********************** CREATE LIST OVERLAY **************************/
  //Boolean for whether the createList overlay is visible or not
  const [createListVisible, setcreateListOverlay] = useState(false);

  /**
   * Wrapper function for above
   */
  function toggleCreateListOverlay() {
    setcreateListOverlay(!createListVisible);
  }

  /** Variable to store the name of a new list if created */
  var newList = "";

  /** Function to store input from textInput view into variable above */
  function storeNewListInput(input: string) {
    newList = input;
  }

  /**
   * The Create List overlay which pops up when createListOverlayVisible is true. 
   */
  const CreateListOverlay = () => {
    return (
      <Overlay
        isVisible={createListVisible}
        onBackdropPress={toggleCreateListOverlay}
        overlayStyle={appStyle.createListOverlay}
      >
        <SafeAreaView style={appStyle.createListContainer}>
          <Text style={appStyle.createListTitle}> Create a new list</Text>
          <TextInput
            style={appStyle.loginInput}
            placeholder="New list name"
            onChangeText={(input) => storeNewListInput(input)}
            maxLength={15}
          />
          <View style={appStyle.signUpButtonsContainer}>

            <TouchableHighlight
              // save button creates list and exits overlay
              style={appStyle.createListSaveButton}
              onPress={() => {
                createBlankListWithName(newList);
                toggleCreateListOverlay();
                toggleListDropDownOverlay();
              }}
              underlayColor="#a9d18e"
            >
              <Text style={appStyle.signUpButtonText}>Save</Text>
            </TouchableHighlight>
            <TouchableHighlight
              // Cancel button just exits this overlay
              style={appStyle.createListCancelButton}
              underlayColor="#a9d18e"
              onPress={() => toggleCreateListOverlay()}
            >
              <Text style={appStyle.loginButtonText}>Cancel</Text>
            </TouchableHighlight>

          </View>
        </SafeAreaView>
      </Overlay>
    );
  };

  /********************** DUPLICATE LIST OVERLAY **************************/
  //Boolean for whether the duplicate list overlay is visible or not
  const [duplicateListVisible, setduplicateListOverlay] = useState(false);

  /**
   * Wrapper function for above
   */
  function toggleDuplicateListOverlay() {
    setduplicateListOverlay(!duplicateListVisible);
  }

  /** Variable to store the name of a new list if created */
  var duplicateName = "";

  /** Function to store input from textInput view into variable above */
  function storeDuplicateListInput(input: string) {
    duplicateName = input;
  }

  /*Search for list and duplicate */
  const DuplicateListOverlay = () => {
    return (
      <Overlay
        isVisible={duplicateListVisible}
        onBackdropPress={toggleDuplicateListOverlay}
        overlayStyle={appStyle.createListOverlay}
      >
        <SafeAreaView style={appStyle.createListContainer}>
          <Text style={appStyle.createListTitle}>New name for duplicated list:</Text>
          <TextInput
            style={appStyle.createListInput}
            placeholder="List name"
            onChangeText={(input) => storeDuplicateListInput(input)} 
            maxLength={15}
          />
          <View style={appStyle.createListButtonContainer}>

            <TouchableHighlight
              // checks the list, duplicates and exits overlay
              style={appStyle.createListSaveButton}
              onPress={() => {
                duplicateFilledList(duplicateName);
                toggleDuplicateListOverlay();
                togglePenDropDownOverlay();
              }}
              underlayColor="#acc49b"
            >
              <Text style={appStyle.createListSaveButtonText}>Duplicate</Text>
            </TouchableHighlight>

            <TouchableHighlight
              // Cancel button just exits this overlay
              style={appStyle.createListCancelButton}
              underlayColor="#acc49b"
              onPress={() => toggleDuplicateListOverlay()}
            >
              <Text style={appStyle.createListCancelButtonText}>Cancel</Text>
            </TouchableHighlight>

          </View>
        </SafeAreaView>
      </Overlay>
    );
  };

/********************** Rename LIST OVERLAY **************************/
  //Boolean for whether the rename list overlay is visible or not
  const [renameListVisible, setRenameListOverlay] = useState(false);

  /**
   * Wrapper function for above
   */
  function toggleRenameListOverlay() {
    setRenameListOverlay(!renameListVisible);
  }

  /** Variable to store the name of a new list if created */
  var reName = "";

  /** Function to store input from textInput view into variable above */
  function storeRenameListInput(input: string) {
    reName = input;
  }

  /**
   * Checks to see if the current list has any items inside, if all clear then duplicate the current list.
   * Refreshes list info on page.
   * Important! Doesn't validate name. Must be done before calling.
   * @param name - Must not be name of a pre-existing list, otherwise won't switch to that list.
   */
  async function renameList(name: string) {
    if (name == "") {
      Alert.alert("Can't have a blank list name!");
      return;
    }
    const db_renameList : any = await fetch('https://deco3801-coding-at-the-disco.uqcloud.net/api/lists/rename_list.php', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        listID: allLists[currentList][0].productID,
        newName: name
      })
    })
    //Catch and alert the user if the duplicate list API fetch call failed.
    .catch((error) => Alert.alert(error.toString()));
    const db_response = await db_renameList;
    if (db_response.status === 400) {
      Alert.alert("Rename List fail");
    }
    //Refresh page to show updated list.
    refreshLists();
  }

  /*Search for list and duplicate */
  const RenameListOverlay = () => {
    return (
      <Overlay
        isVisible={renameListVisible}
        onBackdropPress={toggleRenameListOverlay}
        overlayStyle={appStyle.createListOverlay}
      >
        <SafeAreaView style={appStyle.createListContainer}>
          <Text style={appStyle.createListTitle}>New name for current list:</Text>
          <TextInput
            style={appStyle.createListInput}
            placeholder="List name"
            onChangeText={(input) => storeRenameListInput(input)} 
            maxLength={15}
          />
          <View style={appStyle.createListButtonContainer}>

            <TouchableHighlight
              // checks the list, duplicates and exits overlay
              style={appStyle.createListSaveButton}
              onPress={() => {
                renameList(reName);
                toggleRenameListOverlay();
                togglePenDropDownOverlay();
              }}
              underlayColor="#acc49b"
            >
              <Text style={appStyle.createListSaveButtonText}>Rename</Text>
            </TouchableHighlight>

            <TouchableHighlight
              // Cancel button just exits this overlay
              style={appStyle.createListCancelButton}
              underlayColor="#acc49b"
              onPress={() => toggleRenameListOverlay()}
            >
              <Text style={appStyle.createListCancelButtonText}>Cancel</Text>
            </TouchableHighlight>

          </View>
        </SafeAreaView>
      </Overlay>
    );
  };
  
/********************** DELETE LIST OVERLAY **************************/
  //Boolean for whether the duplicate list overlay is visible or not
  const [deleteListVisible, setdeleteListOverlay] = useState(false);

  /**
   * Wrapper function for above
   */
  function toggleDeleteListOverlay() {
    setdeleteListOverlay(!deleteListVisible);
  }

  /**
   * The Delete List overlay which pops up when deleteListVisible is true. 
   */
  const DeleteListOverlay = () => {
    return (
      <Overlay
        isVisible={deleteListVisible}
        onBackdropPress={toggleDeleteListOverlay}
        overlayStyle={appStyle.buttonContainerOverlay}
      >
        <View>
          <Text style={{alignSelf:'center', fontSize: 20}}>Delete the list?</Text>
          <View style={appStyle.buttonContainer}>
            <Button
            //The user is asked a second time to limit the amount of misclicks and mistakes.
              title="Yes" 
              onPress={() => {
                deleteList()
                toggleDeleteListOverlay(); //closes pop up
                togglePenDropDownOverlay(); //closes pen menu
              }}
              color="#70ad47"
            />
          <View style={appStyle.buttonContainer}></View>
            <Button
            //When the user does not want to delete the list, no can always be pressed to safely exit.
              title="No"
              onPress={() => {toggleDeleteListOverlay()}} //closes pop up
              color="#548235"
            />
          </View>
        </View>
      </Overlay>
      );
    };
  
  /********************** SHARE LIST OVERLAY **************************/
  //Boolean for whether the duplicate list overlay is visible or not
  const [shareListVisible, setShareListOverlay] = useState(false);

  /**
   * Wrapper function for above
   */
  function toggleShareListOverlay() {
    setShareListOverlay(!shareListVisible);
  }

  /** Variable to store the name of a new list if created */
  var accountName = "";

  /** Function to store input from textInput view into variable above */
  function storeAccountInformation(input: string) {
    accountName = input;
  }

  /**
   * The Share List overlay which pops up when shaerListVisible is true. 
   */
  const ShareListOverlay = () => {
    return (
      <Overlay
        isVisible={shareListVisible}
        onBackdropPress={toggleShareListOverlay}
        overlayStyle={appStyle.createListOverlay}
      >
        <SafeAreaView style={appStyle.createListContainer}>
          <Text style={appStyle.createListTitle}>With who?</Text>
          <TextInput
            style={appStyle.createListInput}
            placeholder="Email" //account id
            onChangeText={(input) => storeAccountInformation(input)}
            maxLength={15}
          />
          <View style={appStyle.createListButtonContainer}>

            <TouchableHighlight
              // checks the list, duplicates and exits overlay
              style={appStyle.createListSaveButton}
              onPress={() => {
                duplicateFilledList(newList);
                toggleShareListOverlay();
              }}
              underlayColor="#acc49b"
            >
              <Text style={appStyle.createListSaveButtonText}>Send</Text>
            </TouchableHighlight>
            <TouchableHighlight
              // Cancel button just exits this overlay
              style={appStyle.createListCancelButton}
              underlayColor="#acc49b"
              onPress={() => toggleShareListOverlay()}
            >
              <Text style={appStyle.createListCancelButtonText}>Cancel</Text>
            </TouchableHighlight>
          </View>
        </SafeAreaView>
      </Overlay>
    );
  };

  /********************** SELECT LIST DROPDOWN **************************/
  //Boolean for whether the listDropDown overlay is visible or not.
  const [listDropDownVisible, setListDropDownOverlay] = useState(false);

  /**
   * Wrapper function for above
   */
  function toggleListDropDownOverlay() {
    setListDropDownOverlay(!listDropDownVisible);
  }

  /**
   * Separator for drop down
   */
  const dropDownItemSeparator = () => {
    return <View style={appStyle.listDropDownSeparator} />;
  };

  /**
   * The List Drop Down overlay which pops up when listDropDownVisible is true. 
   */
  const ListDropDownOverLay = () => {
    return (
      <Overlay
        isVisible={listDropDownVisible}
        onBackdropPress={toggleListDropDownOverlay}
        overlayStyle={appStyle.listDropDownOverlay}
      >
        <View style={appStyle.listDropDownContainer}>
          <FlatList
            style={appStyle.listDropDownFlatList}
            ItemSeparatorComponent={dropDownItemSeparator}
            data={allLists}
            keyExtractor={(list) => list[0].name}
            renderItem={({ item }) => (
              <TouchableHighlight
                underlayColor="#acc49b"
                onPress={() => {
                  switchToList(item[0].name); // display chosen list
                  toggleListDropDownOverlay(); // close dropdown
                }}
              >
                <View style={appStyle.listDropDownItem}>
                  <Text style={appStyle.listDropDownText}>{item[0].name}</Text>
                </View>
              </TouchableHighlight>
            )}
          />

          <TouchableHighlight // Create list "plus" button
            underlayColor="#acc49b"
            onPress={toggleCreateListOverlay}
          >
            <Ionicons
              size={50}
              style={appStyle.addListButton}
              name="md-add"
              color="white"
            />
          </TouchableHighlight>
        </View>
      </Overlay>
    );
  };

  /********************** PEN BUTTON DROPDOWN **************************/
  //Boolean for whether the penDropDown overlay is visible or not.
  const [penDropDownVisible, setPenDropDownOverlay] = useState(false);

  /**
   * Wrapper function for above
   */
  function togglePenDropDownOverlay() {
      setPenDropDownOverlay(!penDropDownVisible);
  }

  /**
   * Separator for drop down
   */
  const dropDownListSeparator = () => {
    return <View style={appStyle.listDropDownSeparator} />;
  };

   /**
   * The Pen Drop Down overlay which pops up when penDropDownVisible is true. 
   */
  const PenDropDownOverLay = () => {
    return (
      <Overlay
        isVisible={penDropDownVisible}
        onBackdropPress={togglePenDropDownOverlay}
        overlayStyle={appStyle.penDropDownOverlay}
      >
        <View style={appStyle.buttonContainer}>
          <Button
          //Button to toggleDuplicateListOverlay
            title = "Duplicate"
            onPress = {() => {
              toggleDuplicateListOverlay(); // open list overlay
            }}
            color = "#548235"
          />
           <View style={appStyle.buttonContainer}></View>
           <Button
           //Button to toggleDeleteListOverlay
              title = "Share List"
              onPress = {() => {
                toggleShareListOverlay(); //open share list  
              }}
              color = "#548235"
              />
              <View style={appStyle.buttonContainer}></View>
              <Button
              //Button to toggleDeleteListOverlay
                 title = "Rename List"
                 onPress = {() => {
                   toggleRenameListOverlay(); //open share list  
                 }}
                 color = "#548235"
                 />
            <View style={appStyle.buttonContainer}></View>
            <Button
            //Button to toggleShareListOverlay
              title = "Delete List"
              onPress = {() => {
                toggleDeleteListOverlay(); // delete list
              }}
              color = "crimson"
              />
        </View>
      </Overlay>
    );
  };
  
  /********************** FILTER BUTTON DROPDOWN **************************/
  //Boolean for whether the listDropDown overlay is visible or not.
  const [filterDropDownVisible, filterDropDownOverlay] = useState(false);

  /**
   * Wrapper function for above
   */
  function toggleFilterDropDownOverlay() {
    filterDropDownOverlay(!filterDropDownVisible);
  }

  // Store filters 
  const [colesSelected, setColes] = useState(true);
  const [aldiSelected, setAldi] = useState(true);
  const [woolSelected, setWool] = useState(true);

  // Apply the filters to the products 
  function applyFilters() {
    refreshProductsWithStores(aldiSelected, colesSelected, woolSelected, minPrice, maxPrice);
    toggleFilterDropDownOverlay(); 
  }

  /**
   * The Filter Drop Down overlay which pops up when filterDropDownVisible is true. 
   */
  const FilterDropDownOverLay = () => {
    return (
      <Overlay
        isVisible={filterDropDownVisible}
        onBackdropPress={toggleFilterDropDownOverlay}
        overlayStyle={appStyle.filterDropDownContainer}
      >
        <SafeAreaView>
          <Text style={appStyle.filterText}>Filter by stores:</Text>

          <View style={appStyle.filterStoreContainer}>

          <TouchableHighlight
            underlayColor="#70ad47"
            onPress={() => setAldi(!aldiSelected)}
            style={aldiSelected ? appStyle.filterStoreSelected: appStyle.filterStoreNotSelected}
          >
              <Text style={aldiSelected ? appStyle.filterStoreSelectedText: appStyle.filterStoreNotSelectedText}>Aldi</Text>
          </TouchableHighlight>

          <TouchableHighlight
            underlayColor="#70ad47"
            onPress={() => setWool(!woolSelected)}
            style={woolSelected ? appStyle.filterStoreSelected: appStyle.filterStoreNotSelected}
          >
              <Text style={woolSelected ? appStyle.filterStoreSelectedText: appStyle.filterStoreNotSelectedText}>Woolworths</Text>
          </TouchableHighlight>

          <TouchableHighlight
            underlayColor="#70ad47"
            onPress={() => setColes(!colesSelected)}
            style={colesSelected ? appStyle.filterStoreSelected: appStyle.filterStoreNotSelected}
          >
              <Text style={colesSelected ? appStyle.filterStoreSelectedText: appStyle.filterStoreNotSelectedText}>Coles</Text>
          </TouchableHighlight>

          </View>
          <Text style={appStyle.filterText}>Filter by price:</Text>
          
          
          <View style={appStyle.filterPriceContainer}>
            <View style = {appStyle.filterPriceInputContainer}>

              <Text style={appStyle.filterPriceText}>$</Text>
                <TextInput
                  style={appStyle.filterPriceInput}              
                  keyboardType='number-pad'
                  onChangeText={text => setMinPrice(Number(text))}
                  value={String(minPrice)}
                  maxLength={3}
                />
            </View>

            <Ionicons
              size={30}
              style={appStyle.filterPriceRangeIcon}
              name="md-remove"
            />

            <View style = {appStyle.filterPriceInputContainer}>
              <Text style={appStyle.filterPriceText}>$</Text>

              <TextInput
                style={appStyle.filterPriceInput}              
                keyboardType='number-pad'
                onChangeText={text => setMaxPrice(Number(text))}
                value={String(maxPrice)}
                maxLength={3}
                />
            </View>
          </View>

          

          {/*
          <Slider
            style={appStyle.filterSliderContainer}
            value={maxPrice}
            onSlidingComplete={(value) => setMaxPrice(value)}
            maximumValue={500}
            minimumValue={10}
            step={5}
            trackStyle={{ height: 10, backgroundColor: "transparent" }}
            thumbStyle={{
              height: 20,
              width: 20,
              backgroundColor: "transparent",
            }}
            thumbProps={{
              children: (
                <Icon
                  name="dollar"
                  type="font-awesome"
                  size={15}
                  reverse
                  containerStyle={{ bottom: 15, right: 15 }}
                  color="#C5E0B4"
                />
              ),
            }}
            
          />
          */}

          <TouchableHighlight
            style={appStyle.filterApplyButton}
            underlayColor="#70ad47"
            onPress={applyFilters}
          >
            <Text style={appStyle.filterApplyButtonText}>Apply Filter</Text>
          </TouchableHighlight>
        </SafeAreaView>
      </Overlay>
    );
  };

  /**
   * The overlay for removing items.
   */
  const RemoveItemOverlay = () => {
    /**
     * Go through current list and remove selected items,
     * then refresh & detoggle overlay.
     */
    function removeSelectedItems() {
      for (var i = 0; i < allLists[currentList].length; i++) {
        //Add item to list, unless its the items to be removed.
        if (allLists[currentList][i].selected == true) {
          removeItem(allLists[currentList][i].productID.toString());
        }
      }
      refreshLists();
      toggleRemoveItemOverlay();
    }
    /**
     * Overlay "html".
     */
    return (
      <Overlay
        overlayStyle={appStyle.buttonContainerOverlay}
        onBackdropPress={toggleRemoveItemOverlay}
        isVisible={removeItemOverlayVisible}
      >
        <View>
          <Text style={{alignSelf:'center', fontSize: 20}}>Remove all selected items?</Text>
          <View style={appStyle.buttonContainer}>
            <Button
              testID={"submit remove item button"}
              title="Yes" 
              onPress={removeSelectedItems}
              color="#70ad47"
            />
          <View style={appStyle.buttonContainer}></View>
            <Button
              title="No"
              onPress={toggleRemoveItemOverlay}
              color="#548235"
            />
          </View>
        </View>
      </Overlay>
    );
  };

  //The value for whether this screen is search on display list, false == list
  const [switcheroo, togglyboo] = useState(false);

  /**
   * Toggle screen.
   */
  function toggleeryoo() {
    if (!switcheroo) {
      refreshProductsWithStores(true, true, true, minPrice, maxPrice);
    }
    togglyboo(!switcheroo);
  }

  function countDecimals(val: number) {
    if(Math.floor(val) === val) return 0;
    var strVal = val + "";
    if (strVal.indexOf(".") == -1) {
      return 0;
    } else {
      return strVal.split(".")[1].length || 0; 
    }
  }

  function price(value: number) {
    switch (countDecimals(value)) {
      case 0:
        return value + ".00";
      case 1:
        return value + "0";
      case 2:
        return value + "";
      default:
        break;
    }
  }

  /**
   * The screen.
   * It's divided between add item (or search item) and view list
   * screen. User toggles with plus and back buttons.
   */
  if (switcheroo) {
    //important, dont put other things here
    return (
      <View style={appStyle.allScreen}>
        <View style={appStyle.headerBar}>

          {/**Filter list dropdown**/}
          <FilterDropDownOverLay />

          <Ionicons
            size={50}
            style={{ marginBottom: -3 }}
            name="ios-arrow-dropleft-circle"
            color="white"
            onPress={toggleeryoo}
          />
          <Text style={appStyle.title}>Search</Text>
          <Ionicons
            size={50}
            style={{ marginBottom: -3 }}
            name="ios-funnel"
            color="white"
            onPress={toggleFilterDropDownOverlay}
          />
        </View>
        
        <SearchBar
          placeholder="Search for an item"
          onChangeText={updateSearchContents}
          onSubmitEditing={() => refreshProductsWithStores(true, true, true, minPrice, maxPrice)}
          value={searchBarContents}
          inputStyle={{ paddingLeft: 60}}
          lightTheme={true}
          platform={"android"}
          round={true}
        />
        
        <View style={styles.divider}></View>
          
          <ScrollView contentContainerStyle={{flexGrow: 1}}>
            {(productData.length == 0) ? <Text style={{paddingHorizontal:"10%"}}>No Items match those criteria. Please change your search or filter settings and try again.</Text> :(callOnceAtStart || (productData[0].price == -1)) ? <View><ActivityIndicator/><Text>Loading...</Text></View> : 
              <View>
                <Text style={styles.categoryHeaderText}>Recently added items</Text>
                <FlatList
                  horizontal
                  data={productData}
                  style={{marginLeft:'3%'}}
                  keyExtractor={product => product.id}
                  showsHorizontalScrollIndicator={false}
                  renderItem={({item}) => {
                    return (
                      <View style={styles.productContainer}>
                        <View style={styles.productSecondaryContainer}>
                          <Text style={{color: "#548235", fontWeight:'bold', paddingBottom: 5}}>{item.name}</Text>
                          <Text style={{color: "#70ad47", paddingBottom: 5}}>{item.description}</Text>
                          <Text style={{color: "#548235", fontWeight:'bold', paddingBottom: 5}}>${price(item.price)}</Text>
                          <Text style={{color: "#70ad47", paddingBottom: 10}}>Available at: </Text>
                          <View style={{flexDirection:'row'}}>
                            {item.atColes ? <Text style={styles.productText}>Coles</Text> : null}
                            {item.atWoolworths ? <Text style={styles.productText}>Woolworths</Text> : null}
                            {item.atAldi ? <Text style={styles.productText}>Aldi</Text> : null}
                          </View>
                          <Ionicons testID={"select item"} size={50} style={{bottom: "5%", right: "10%", position: "absolute"}} name='md-add-circle' color='#70ad47' onPress={()=>toggleAddItemOverlayWithNameAndID(item.name, item.id)}/>
                        </View>
                      </View>
                    );
                  }}
                />
                {/*Insert line here with some margin bottom and top*/}
                <View style={styles.divider}></View>
                <Text style={styles.categoryHeaderText}>Popular items</Text>
                <FlatList
                  horizontal
                  data={productData}
                  style={{/*styles.flatlist*/ width: width, marginLeft:'3%'}}
                  keyExtractor={product => product.id}
                  showsHorizontalScrollIndicator={false}
                  renderItem={({item}) => {
                    return (
                      <View style={styles.productContainer}>
                        <View style={styles.productSecondaryContainer}>
                        <Text style={{color: "#548235", fontWeight:'bold', paddingBottom: 5}}>{item.name}</Text>
                          <Text style={{color: "#70ad47", paddingBottom: 5}}>{item.description}</Text>
                          <Text style={{color: "#548235", fontWeight:'bold', paddingBottom: 5}}>${price(item.price)}</Text>
                          <Text style={{color: "#70ad47", paddingBottom: 10}}>Available at: </Text>
                          <View style={{flexDirection:'row'}}>
                            {item.atColes ? <Text style={styles.productText}>Coles</Text> : null}
                            {item.atWoolworths ? <Text style={styles.productText}>Woolworths</Text> : null}
                            {item.atAldi ? <Text style={styles.productText}>Aldi</Text> : null}
                          </View>
                        <Ionicons size={50} style={{bottom: "5%", right: "10%", position: "absolute"}} name='md-add-circle' color='#70ad47' onPress={()=>toggleAddItemOverlayWithNameAndID(item.name, item.id)}/>
                      </View>
                      </View>
                    );
                  }}
                />
                {/*Insert line here with some margin bottom and top*/}
                <View style={styles.divider}></View>
                <Text style={styles.categoryHeaderText}>Frequently added items</Text>
                <FlatList
                  horizontal
                  data={productData}
                  style={{/*styles.flatlist*/ width: width, marginLeft:'3%'}}
                  keyExtractor={product => product.id}
                  showsHorizontalScrollIndicator={false}
                  renderItem={({item}) => {
                    return (
                      <View style={styles.productContainer}>
                        <View style={styles.productSecondaryContainer}>
                        <Text style={{color: "#548235", fontWeight:'bold', paddingBottom: 5}}>{item.name}</Text>
                          <Text style={{color: "#70ad47", paddingBottom: 5}}>{item.description}</Text>
                          <Text style={{color: "#548235", fontWeight:'bold', paddingBottom: 5}}>${price(item.price)}</Text>
                          <Text style={{color: "#70ad47", paddingBottom: 10}}>Available at: </Text>
                          <View style={{flexDirection:'row'}}>
                            {item.atColes ? <Text style={styles.productText}>Coles</Text> : null}
                            {item.atWoolworths ? <Text style={styles.productText}>Woolworths</Text> : null}
                            {item.atAldi ? <Text style={styles.productText}>Aldi</Text> : null}
                          </View>
                        <Ionicons size={50} style={{bottom: "5%", right: "10%", position: "absolute"}} name='md-add-circle' color='#70ad47' onPress={()=>toggleAddItemOverlayWithNameAndID(item.name, item.id)}/>
                      </View>
                      </View>
                    );
                  }}
                />
            </View>
           }
          </ScrollView>
          {/**
           * Overlays section. Should all be hidden initially. 
           * 
           * Overlays are also a good example of how to create ur own custom "html" component.
           */}
          <AddItemOverlay/>
        </View>
    );
  } else {
    return (
      <View style={appStyle.allScreen}>
        {/** List dropdown section hidden until clicked */}
        <ListDropDownOverLay />

        {/** Login Overlay shown at the start */}
        <LoginOverlay />

        {/** Login Overlay shown at the start */}
        <SignUpOverlay />

        {/** Welcome mat overlay shown at the start */}
        <WelcomeMat/>
        {/** Pen Button Dropdown shown at the start */}
        <PenDropDownOverLay />

        {/** Duplicate shown at the start */}
        <DuplicateListOverlay />

        {/** DeleteList shown at the start */}
        <DeleteListOverlay/>

        {/** DeleteList shown at the start */}
        <ShareListOverlay/>

        {/** RenameList shown at the start */}
        <RenameListOverlay/>

        {/**
         * The header.
         */}
        <View style={appStyle.headerBar}>
          <Ionicons
            size={50}
            style={{ marginBottom: -10 }}
            name="ios-arrow-down"
            color="white"
            onPress={() => toggleListDropDownOverlay()}
          />
          <Text testID="list title" style={styles.listName}>{allLists[currentList][0].name}</Text> 
          <Ionicons
            size={50}
            style={{ marginBottom: -5 }}
            name="md-create"
            color="white"
            onPress={() => togglePenDropDownOverlay()}
          />
        </View>

        {/** Create list overlay */}
        <CreateListOverlay />

        <Ionicons
          testID={"add item button"}
          size={50}
          style={{
            marginBottom: -3,
            bottom: "5%",
            right: "10%",
            position: "absolute",
            zIndex: 2,
          }}
          name="md-add-circle"
          color="#70ad47"
          onPress={toggleeryoo}
        />

        {/**
         * Floating delete button.
         */}
        <Ionicons
          testID={"remove item button"}
          size={50}
          style={{
            marginBottom: -3,
            bottom: "5%",
            left: "10%",
            position: "absolute",
            zIndex: 2,
          }}
          name="md-remove-circle"
          color="#70ad47"
          onPress={toggleRemoveItemOverlay}
        />

        {/**
         * The Main content area between header and bottom navigation bar.
         */}
        <ScrollView contentContainerStyle={appStyle.centralContainer}>
          {/*The aforementioned current list*/}
          {listItems}
          {/*Just need this as a buffer cos last item doesn't get all the way on the screen when enough items are in the list that scrolling is needed.*/}
          <View style={{ padding: "5%" }}></View>
        </ScrollView>

        {/**
         * Overlays section. Should all be hidden initially.
         */}
        <AddItemOverlay/>

        <RemoveItemOverlay />
      </View>
    );
  }
}

//This screen's styles
const styles = StyleSheet.create({
  checkboxContainer: {
    flexDirection: "row",
    padding: "5%",
    width: "100%",
    backgroundColor: "limegreen",
  },
  //The style for productContainer
  productContainer: {
    flex:1,
    backgroundColor: "transparent",
    paddingLeft: "10%",
    paddingVertical: "5%",
    width: 240,
    height: 380,
  },
  //The style for productSecondaryContainer
  productSecondaryContainer: {
    borderRadius: 15,
    borderWidth: 1,
    borderColor: "#70ad47",
    backgroundColor: "white",
    padding: "5%",
    flex: 1,
    elevation: 3
  },
  //The style for productText
  productText: {
    marginRight:'6%',
    borderRadius: 5,
    backgroundColor: "#a9d18e",
    padding: '3%',
    color: "white", 
  },
  //The style for checkbox
  checkbox: {
    maxHeight: "100%",
    alignSelf: "center",
  },
  //The style for item
  item: {
    flexDirection: "row",
    textAlignVertical: "center",
    width: "5%",
    height: "100%",
    fontSize: 15,
    flexGrow: 2,
  },
  //The style for itemQuantity
  itemQuantity: {
    flex: 1,
    flexDirection: "row-reverse",
    flexGrow: 1,
  },
  //The style for pickerStyle
  pickerStyle: {
    alignSelf: "center",
    width: "85%",
    color: "grey",
  },
  //The style for list
  list: {
    alignSelf: "center",
    flexDirection: "row",
    width: "100%",
    height: "10%",
    borderBottomWidth: 1,
    borderBottomColor: "lightskyblue",
  },
  //The style for listActive
  listActive: {
    alignSelf: "center",
    flexDirection:"row",
    width: "100%",
    height: "10%",
    backgroundColor: "#A9D18E",
    borderBottomWidth: 1,
    borderBottomColor: "skyblue",
  },
  //The style for listName
  listName: {
    color: "white",
    fontSize: 30,
    justifyContent: "center",
    alignSelf: "center",
    flexDirection: "row",
  },
  categoryHeaderText: {
    color: "#70ad47",
    fontWeight: "bold",
    fontSize: 22,
    textAlign: "center",
  },
  divider: {
    borderColor: "#70ad47",
    borderWidth: 1, 
    marginBottom:'2%', 
    marginTop:'5%', 
    width:'80%', 
    alignSelf:'center',
  },
  addItemText: {
    color: "#548235",
    fontSize: 20,
    fontWeight: "bold",
    paddingVertical: 5,
  },
});
