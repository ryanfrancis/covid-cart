import React, { useState, Component } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View, Button, Alert, CheckBox, ScrollView } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';
import appStyle from './appStyle'
import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import {SearchBar} from 'react-native-elements'

//ignore this whole file. literally close this now.


export default function SearchItemsScreen() {
  //Retrieve data from database

  //Convert to below format (or whatever format we decide on)
  const [itemData, changeData] = useState(
    {
      "Recently Added items":[
        {name:"Generic Cereal Bread", description:"High fibre breakfast cereal 730g", price:7.50},//stores (however that's stored (pun intended))
        {name:"White Bread", description:"700g of the whitest bread", price:3.40}
      ],
      "Popular items":[
        {}
      ],
      //other, distinctly predefined categories
    }
  );

  const [postText, setPostText] = useState('Stuff');
  /**
   * Need this to navigate between screens:
   *    navigation.Navigate('ScreenName'), or
   *    navigation.goBack().
   * See BottomTabNavigator.tsx for bottom tab screen names. 
   */
  const navigation = useNavigation();
  //()=>navigation.setParams({post:postText});
  

  //Var for contents of search bar
  const [searchBarContents, updateSearchContents] = useState("");

  /**
   * Searches for an item.
   * Called when user presses enter on search bar.
   * Uses contents of search bar at that time.
   */
  function search() {
    //Replace with submit query to db.
    Alert.alert("Searched for '" + searchBarContents + "'");
  }

  //, {screen:"Lists", params:{post: postText}}

  return (
    <View style = {appStyle.allScreen}>


      <View style = {appStyle.headerBar}>
        <Ionicons size={50} style={{ marginBottom: -3 }} name='ios-arrow-dropleft-circle' color='white' onPress={() => navigation.navigate('Lists', {name:"john"})}/>
        <Text style={appStyle.title}>Search</Text>
        <Ionicons size={50} style={{ marginBottom: -3 }} name='ios-funnel' color='white' onPress={() => Alert.alert('Filter Button Pressed')}/>
      </View>

      <SearchBar
        placeholder="Search for an item"
        onChangeText={updateSearchContents}
        onSubmitEditing ={search}
        value={searchBarContents}
        containerStyle={{borderRadius:15, width:"80%", marginTop:10}}
        inputStyle={{textAlign:'center'}}
        lightTheme={true}
        round={true}
      />

      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        {/*flatlists*/}
      </ScrollView>


    </View>
  );
}


const styles = StyleSheet.create({

});
