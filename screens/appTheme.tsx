import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import TabThreeScreen from './TabThreeScreen';
import TabOneScreen from './TabOneScreen';
import { AppearanceProvider, useColorScheme } from 'react-native-appearance';
import { DefaultTheme, DarkTheme } from '@react-navigation/native';

export default function themeChange() {
    const scheme = useColorScheme();
    const Stack = createStackNavigator();

    return (
        <AppearanceProvider>
            <NavigationContainer theme={scheme === "dark" ? DarkTheme : DefaultTheme}>
                <Stack.Navigator mode="modal">
                <Stack.Screen name="screen1" component={TabThreeScreen} />
                <Stack.Screen name="screen2" component={TabOneScreen} />
                </Stack.Navigator>
            </NavigationContainer>
        </AppearanceProvider>
    )
  } 