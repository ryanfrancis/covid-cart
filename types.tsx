export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
  AddItem: undefined;
};

export type BottomTabParamList = {
  Lists: undefined;
  Stores: undefined;
  Settings: undefined;
};

export type TabOneParamList = {
  TabOneScreen: undefined;
  //test: Object;
};

export type TabTwoParamList = {
  TabTwoScreen: undefined;
};

export type TabThreeParamList = {
  TabThreeScreen: undefined;
};