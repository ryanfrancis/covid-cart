import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { View } from './components/Themed';
import { Text, ActivityIndicator } from 'react-native';

import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';

export default function App() {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();
  console.disableYellowBox = true; //<-Suppres Warnings

  if (!isLoadingComplete) {
    return (
      <View>
        <ActivityIndicator/>
        <Text>Loading...</Text>
      </View>
    );
  } else {
    return (
      <SafeAreaProvider>
        <Navigation colorScheme={colorScheme}/>
        <StatusBar/>
      </SafeAreaProvider>
    );
  }
}
